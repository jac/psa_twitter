In this repository there are two scripts:

1. **script_1file.R** parses a `json` file with the accounts that one given account is following in Twitter and gives two numbers: the number of accounts following that are included in a master file called `psa_handles.csv` and the number of total accounts followed by that account.
2. **script_1dir.R** does the same, but for a complete set of files stored in the `raw_data/json_files` directory, and it provides a table with the results for each of those files, it also saves the table in R format in the `rda`directory for subsequent treatment.
